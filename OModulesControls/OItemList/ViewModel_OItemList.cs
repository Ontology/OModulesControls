﻿using OModulesControls.BaseClasses;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace OModulesControls.OItemList
{
    [Flags]
    public enum ItemType
    {
        Objects = 1,
        ObjectRelationLeftRight = 2,
        ObjectRelationRightLeft = 4
    }
    public class ViewModel_OItemList : ViewModelBase
    {
        private clsLocalConfig localConfig;
        private ItemType itemType;
        private Globals globals;
        public Globals Globals 
        {
            get { return globals; }
            set
            {
                globals = value;
                localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
                if (localConfig == null)
                {
                    localConfig = new clsLocalConfig(new Globals());
                    LocalConfigManager.AddLocalConfig(localConfig);
                }

                dataCollector_OItemList = new DataCollector_OItemList(localConfig);
                dataCollector_OItemList.FinishedLoadingObjects += dataCollector_OItemList_FinishedLoadingObjects;
                dataCollector_OItemList.StartedLoadingObjects += dataCollector_OItemList_StartedLoadingObjects;
            }
        }

        private int loadProgress;
        public int LoadProgress
        {
            get { return loadProgress; }
            set
            {
                loadProgress = value;
                RaisePropertyChanged("LoadProgress");
            }
        }

        void dataCollector_OItemList_StartedLoadingObjects(clsOntologyItem parentItem)
        {
            LoadProgress = 50;
        }

        void dataCollector_OItemList_FinishedLoadingObjects(clsOntologyItem parentItem, clsOntologyItem result)
        {
            LoadProgress = 0;
            if (result.GUID == globals.LState_Success.GUID)
            {
                ItemList = new ObservableCollection<dynamic>( dataCollector_OItemList.ObjectList );
            }
         
        }
        private DataCollector_OItemList dataCollector_OItemList;

        private ObservableCollection<dynamic> itemList;
        public ObservableCollection<dynamic> ItemList 
        {
            get { return itemList; }
            set 
            { 
                itemList = value;
                RaisePropertyChanged("ItemList");
                RaisePropertyChanged("ItemCount");
            }
        }
        public int ItemCount
        {
            get { return ItemList.Count; }
        }

        private string label_ItemCount;
        public string Label_ItemCount
        {
            get { return label_ItemCount; }
            set
            {
                label_ItemCount = value;
                RaisePropertyChanged("Label_ItemCount");
            }
        }

        private string label_GUID;
        public string Label_GUID
        {
            get { return label_GUID; }
            set
            {
                label_GUID = value;
                RaisePropertyChanged("Label_GUID");
            }
        }

        private string gUID_Selected;
        public string GUID_Selected
        {
            get { return gUID_Selected; }
            set 
            { 
                gUID_Selected = value;
                RaisePropertyChanged("GUID_Selected");
            }
        }

        public void SetGuidOfSelected(SelectedCellsChangedEventArgs e)
        {
            
            switch (itemType)
            {
                case ItemType.Objects:
                    clsOntologyItem objectItem = e.AddedCells.Count == 1 ? (clsOntologyItem)e.AddedCells[0].Item : null;

                    if (objectItem != null)
                    {
                        GUID_Selected = objectItem.GUID;
                    }
                    break;
                case ItemType.ObjectRelationLeftRight:

                    break;
                case ItemType.ObjectRelationRightLeft:

                    break;
                default:

                    break;
            }
        }

        public void SetGuidOfSelected(SelectionChangedEventArgs e)
        {
            GUID_Selected = null;
            switch (itemType)
            {
                case ItemType.Objects:
                    clsOntologyItem objectItem = e.AddedItems.Count == 1 ? (clsOntologyItem)e.AddedItems[0] : null;

                    if (objectItem != null)
                    {
                        GUID_Selected = objectItem.GUID;
                    }
                    break;
                case ItemType.ObjectRelationLeftRight:
                    
                    break;
                case ItemType.ObjectRelationRightLeft:
                    
                    break;
                default:
                    
                    break;
            }
        }

        public Visibility VisibilityOfColumn(DataGridAutoGeneratingColumnEventArgs e)
        {
            switch (itemType)
            {
                case ItemType.Objects:
                    if (e.PropertyName == "Name")
                    {
                        return Visibility.Visible;
                    }
                    else
                    {
                        return Visibility.Collapsed;
                    }
                    break;
                case ItemType.ObjectRelationLeftRight:
                    if (e.PropertyName == "Name_Object" ||
                        e.PropertyName == "Name_RelationType" ||
                        e.PropertyName == "OrderID")
                    {
                        return Visibility.Visible;
                    }
                    else
                    {
                        return Visibility.Collapsed;
                    }
                    break;
                case ItemType.ObjectRelationRightLeft:
                    if (e.PropertyName == "Name_Other" ||
                        e.PropertyName == "Name_RelationType" ||
                        e.PropertyName == "OrderID")
                    {
                        return Visibility.Visible;
                    }
                    else
                    {
                        return Visibility.Collapsed;
                    }
                    break;
                default:
                    return Visibility.Collapsed;
                    break;
            }
        }

        public ViewModel_OItemList()
        {
            Initialize();
            label_ItemCount = "Count:";
            label_GUID = "GUID (selected):";

        }

        public void GetData_Objects(clsOntologyItem searchItem)
        {
            itemType = ItemType.Objects;
            dataCollector_OItemList.GetData_Objects(searchItem);
        }

        private void Initialize()
        {
            ItemList = new ObservableCollection<dynamic>();
            
        }
    }
}
