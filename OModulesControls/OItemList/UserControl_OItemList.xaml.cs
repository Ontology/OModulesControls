﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OModulesControls.OItemList
{
    /// <summary>
    /// Interaction logic for UserControl_OItemList.xaml
    /// </summary>
    public partial class UserControl_OItemList : UserControl
    {
        private ViewModel_OItemList viewModel;
        public static readonly DependencyProperty GlobalsProperty = DependencyProperty.Register
            (
                 "Globals", 
                 typeof(Globals), 
                 typeof(UserControl_OItemList), 
                 new PropertyMetadata(null)
            );

        private Globals globals;
        public Globals Globals 
        {
            get { return globals; }
            set
            {
                globals = value;
                viewModel = (ViewModel_OItemList)DataContext;
                viewModel.Globals = globals;
            }
        }

        private void dataGridViewItems_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            e.Column.Visibility = viewModel.VisibilityOfColumn(e);
            
        }

        private void DataGridRow_MouseDoubleClick(object sender, System.Windows.RoutedEventArgs e)
        {
            var row = (DataGridRow)e.Source;
            var oItem = (clsOntologyItem)row.Item;

            var ix = viewModel.ItemList.IndexOf(oItem);
            
        }

        public UserControl_OItemList()
        {
            InitializeComponent();
        }

        public void GetData(clsOntologyItem parentItem)
        {
            viewModel.GetData_Objects(parentItem);
        }

        private void dataGridViewItems_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            viewModel.SetGuidOfSelected(e);
        }

        private void dataGridViewItems_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            viewModel.SetGuidOfSelected(e);
        }
    }
}
