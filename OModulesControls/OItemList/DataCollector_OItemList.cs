﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector;
using System.Threading;

namespace OModulesControls.OItemList
{
    public class DataCollector_OItemList
    {
        public List<clsOntologyItem> ObjectList { get; private set; }

        private clsLocalConfig localConfig;

        private clsOntologyItem searchItemForObjects;

        private OntologyModDBConnector dbConnector_AdvancedFilter_Objects;
        private OntologyModDBConnector dbConnector_ObjectList;

        public delegate void StartLoadingObjects(clsOntologyItem parentItem);
        public event StartLoadingObjects StartedLoadingObjects;

        public delegate void FinishLoadingObjects(clsOntologyItem parentItem, clsOntologyItem result);
        public event FinishLoadingObjects FinishedLoadingObjects;

        public clsOntologyItem AdvancedFilterClass { get; set; }
        public clsOntologyItem AdvancedFilterRelationType { get; set; }
        public clsOntologyItem AdvancedFilterObject { get; set; }
        public clsOntologyItem AdvancedFilterDirection { get; set; }
        public bool DoNullRelationSearch { get; set; }

        public List<clsOntologyItem> ObjectFilterItems { get; set; }
        public string FilterGuid { get; set; }
        public string FilterName { get; set; }

        private Task DataTask_Objects;
        private CancellationToken ctObjects;

        public DataCollector_OItemList(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;

            Initialize();
        }

        private void Initialize()
        {
            dbConnector_ObjectList = new OntologyAppDBConnector.OntologyModDBConnector(localConfig.Globals);
            dbConnector_AdvancedFilter_Objects = new OntologyAppDBConnector.OntologyModDBConnector(localConfig.Globals);
        }

        public clsOntologyItem GetData_Objects(clsOntologyItem searchItem)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            

            searchItemForObjects = searchItem;
            var tokenSource2 = new CancellationTokenSource();
            if (DataTask_Objects == null)
            {
                
                ctObjects = tokenSource2.Token;
                DataTask_Objects = new Task(new Action(GetSubData_Objects), ctObjects);
            }

            
            if (DataTask_Objects.Status != TaskStatus.Created && DataTask_Objects.Status != TaskStatus.Canceled)
            {
                tokenSource2.Cancel();
            }

            tokenSource2.Dispose();
            // Just continue on this thread, or Wait/WaitAll with try-catch: 
            tokenSource2 = new CancellationTokenSource();
            ctObjects = tokenSource2.Token;
            DataTask_Objects = new Task(new Action(GetSubData_Objects), ctObjects);
            DataTask_Objects.Start();

            return result;
        }

        private void GetSubData_Objects()
        {
            var result = localConfig.Globals.LState_Success.Clone();
            if (StartedLoadingObjects != null)
            {
                StartedLoadingObjects(searchItemForObjects);
            }

            if (AdvancedFilterClass != null || AdvancedFilterClass != null || AdvancedFilterObject != null)
            {
                if (AdvancedFilterDirection == null || AdvancedFilterDirection.GUID == localConfig.Globals.Direction_LeftRight.GUID)
                {
                    var searchAdvancedFilter = new List<clsObjectRel> {new clsObjectRel 
                        {
                            ID_Object = searchItemForObjects != null ? searchItemForObjects.GUID : null,
                            ID_Parent_Object = searchItemForObjects != null ? searchItemForObjects.GUID_Parent : null,
                            ID_RelationType = AdvancedFilterRelationType != null ? AdvancedFilterRelationType.GUID : null,
                            ID_Other = AdvancedFilterObject != null ? AdvancedFilterObject.GUID : null,
                            ID_Parent_Other = AdvancedFilterClass != null ? AdvancedFilterClass.GUID : null
                        }};

                    result = dbConnector_AdvancedFilter_Objects.GetDataObjectRel(searchAdvancedFilter);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        if (DoNullRelationSearch)
                        {
                            List<clsOntologyItem> searchObjects;
                            if (searchItemForObjects == null)
                            {
                                searchObjects = null;

                            }
                            else
                            {
                                searchObjects = new List<clsOntologyItem> 
                                {
                                    new clsOntologyItem
                                    {
                                        GUID = searchItemForObjects.GUID,
                                        Name = searchItemForObjects.Name,
                                        GUID_Parent = searchItemForObjects.GUID_Parent
                                    }
                                };
                            }

                            result = dbConnector_ObjectList.GetDataObjects(searchObjects);

                            if (result.GUID == localConfig.Globals.LState_Success.GUID)
                            {
                                ObjectList = (from objectItem in dbConnector_ObjectList.Objects1
                                              join filterItem in dbConnector_AdvancedFilter_Objects.ObjectRels on objectItem.GUID equals filterItem.ID_Object into filterItems
                                              from filterItem in filterItems.DefaultIfEmpty()
                                              where filterItem == null
                                              select objectItem).ToList();
                            }
                        }
                    }
                }
                else if (AdvancedFilterDirection.GUID == localConfig.Globals.Direction_RightLeft.GUID)
                {
                    var searchAdvancedFilter = new List<clsObjectRel> {new clsObjectRel 
                        {
                            ID_Other = searchItemForObjects != null ? searchItemForObjects.GUID : null,
                            ID_Parent_Other = searchItemForObjects != null ? searchItemForObjects.GUID_Parent : null,
                            ID_RelationType = AdvancedFilterRelationType != null ? AdvancedFilterRelationType.GUID : null,
                            ID_Object = AdvancedFilterObject != null ? AdvancedFilterObject.GUID : null,
                            ID_Parent_Object = AdvancedFilterClass != null ? AdvancedFilterClass.GUID : null
                        }};

                    result = dbConnector_AdvancedFilter_Objects.GetDataObjectRel(searchAdvancedFilter);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        if (DoNullRelationSearch)
                        {
                            List<clsOntologyItem> searchObjects;
                            if (searchItemForObjects == null)
                            {
                                searchObjects = null;

                            }
                            else
                            {
                                searchObjects = new List<clsOntologyItem> 
                                {
                                    new clsOntologyItem
                                    {
                                        GUID = searchItemForObjects.GUID,
                                        Name = searchItemForObjects.Name,
                                        GUID_Parent = searchItemForObjects.GUID_Parent
                                    }
                                };
                            }

                            result = dbConnector_ObjectList.GetDataObjects(searchObjects);

                            if (result.GUID == localConfig.Globals.LState_Success.GUID)
                            {
                                ObjectList = (from objectItem in dbConnector_ObjectList.Objects1
                                              join filterItem in dbConnector_AdvancedFilter_Objects.ObjectRels on objectItem.GUID equals filterItem.ID_Other into filterItems
                                              from filterItem in filterItems.DefaultIfEmpty()
                                              where filterItem == null
                                              select objectItem).ToList();
                            }
                        }
                    }
                }
            }
            else
            {
                var searchObjects = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = searchItemForObjects.GUID,
                        Name = searchItemForObjects.Name,
                        GUID_Parent = searchItemForObjects.GUID_Parent
                    }
                };

                result = dbConnector_ObjectList.GetDataObjects(searchObjects);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    ObjectList = dbConnector_ObjectList.Objects1;
                }
            }

            if (FinishedLoadingObjects != null)
            {
                FinishedLoadingObjects(searchItemForObjects, result);
            }
            
        }
    }
}
