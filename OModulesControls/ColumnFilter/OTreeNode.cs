﻿using OModulesControls.BaseClasses;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesControls.ColumnFilter
{
    public class OTreeNode : NotifyPropertyChange
    {
        private bool isExpanded;
        private bool isSelected;

        private clsLocalConfig localConfig;

        private System.Windows.Media.Brush background;

        public bool AutomaticManagement { get; set; }

        public clsOntologyItem Filter { get; set; }
        public clsOntologyItem RefItem { get; set; }

        public string Id 
        {
            get { return Filter.GUID; }
        }
        public string Name 
        {
            get 
            { 
                return Filter.Name; 
            }
            set
            {
                Filter.Name = value;
                RaisePropertyChanged("Name");
            }
        }
        public List<OTreeNode> Nodes { get; set; }
        private clsOntologyItem parentItemOld;
        private OTreeNode parentNode;
        public OTreeNode ParentNode 
        {
            get { return parentNode; }
            set
            {
                if (parentNode != null && parentNode.Id != value.Id)
                {
                    parentItemOld = parentNode.Filter.Clone();
                }
                parentNode = value;
                RaisePropertyChanged("ParentNode");
                if (!AutomaticManagement && Filter.GUID != localConfig.Globals.Root.GUID)
                {
                    SaveFilterToParent();
                }
                
            }
        }
        public bool IsExpanded
        {
            get { return isExpanded; }
            set
            {
                isExpanded = value;
                RaisePropertyChanged("IsExpanded");
            }
        }
        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                isSelected = value;
                RaisePropertyChanged("IsSelected");
            }
        }

        public bool IsRoot
        {
            get { return ParentNode == null ? true : false; }
        }
        public System.Windows.Media.Brush Background 
        {
            get { return background; }
            set
            {
                background = value;
                RaisePropertyChanged("Background");
            }
        }

        public clsOntologyItem SaveFilter()
        {
            localConfig.TransactionWorker.ClearItems();

            var result = localConfig.TransactionWorker.do_Transaction(Filter);
            
            return result;
        }

        public clsOntologyItem SaveFilterToRef()
        {
            localConfig.TransactionWorker.ClearItems();

            var relFilterToRef = localConfig.RelationConfigurator.Rel_ObjectRelation(Filter, RefItem, localConfig.OItem_relationtype_belonging_resource);
            var result = localConfig.TransactionWorker.do_Transaction(relFilterToRef);
            
            return result;
        }

        public clsOntologyItem SaveFilterToParent()
        {
            var result = localConfig.Globals.LState_Success.Clone();
            if (ParentNode != null)
            {
                localConfig.TransactionWorker.ClearItems();
                if (parentItemOld != null)
                {
                    var relParentDelete = localConfig.RelationConfigurator.Rel_ObjectRelation(parentItemOld, Filter, localConfig.OItem_relationtype_contains);
                    result = localConfig.TransactionWorker.do_Transaction(relParentDelete, boolRemoveItem: true);
                }
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    parentItemOld = null;
                    var relFilterToParent = localConfig.RelationConfigurator.Rel_ObjectRelation(ParentNode.Filter, Filter, localConfig.OItem_relationtype_contains);

                    result = localConfig.TransactionWorker.do_Transaction(relFilterToParent);
                }
                
            }

            return result;
        }

        public OTreeNode(clsLocalConfig localConfig)
        {
            Nodes = new List<OTreeNode>();
            Filter = localConfig.Globals.Root;
        }

        public OTreeNode(clsOntologyItem filter, clsOntologyItem refItem, clsLocalConfig localConfig, bool automaticManagement = false)
        {
            RefItem = refItem;
            Filter = filter;
            Nodes = new List<OTreeNode>();
            this.localConfig = localConfig;
            AutomaticManagement = automaticManagement;
        }
    }
}
