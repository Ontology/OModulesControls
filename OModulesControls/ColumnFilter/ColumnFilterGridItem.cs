﻿using OModulesControls.BaseClasses;
using OModulesControls.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace OModulesControls.ColumnFilter
{
    public class ColumnFilterGridItem : NotifyPropertyChange
    {
        [Column(IsVisible=Visibility.Hidden)]
        public string IdColumnFilter
        {
            get { return ProxyItem.IdColumnFilter; }
            set
            {
                ProxyItem.IdColumnFilter = value;
                RaisePropertyChanged("IdColumnFilter");
            }
        }

        [Column(IsVisible = Visibility.Visible,
            DisplayOrder = 100)]
        public string NameColumnFilter
        {
            get { return ProxyItem.NameColumnFilter; }
            //set
            //{
            //    ProxyItem.NameColumnFilter = value;
            //    RaisePropertyChanged("NameColumnFilter");
            //}
        }

        [Column(IsVisible = Visibility.Hidden)]
        public DataGridViewColumn ColumnItem
        {
            get { return ProxyItem.ColumnItem; }
            set
            {
                ProxyItem.ColumnItem = value;
                RaisePropertyChanged("ColumnItem");
                RaisePropertyChanged("IsVisible");
            }
        }

        [Column(IsVisible = Visibility.Visible,
            DisplayOrder = 200)]
        public bool VisibleColumn
        {
            get { return ProxyItem.VisibleColumn; }
            set
            {
                ProxyItem.VisibleColumn = value;
                if (ProxyItem.ColumnItem != null)
                {
                    ProxyItem.ColumnItem.Visible = ProxyItem.VisibleColumn;
                }
                RaisePropertyChanged("VisibleColumn");
            }
        }

        [Column(IsVisible = Visibility.Hidden)]
        public OTreeNode ParentNode
        {
            get { return ProxyItem.ParentNode; }
            set
            {
                ProxyItem.ParentNode = value;
                RaisePropertyChanged("ParentNode");
            }
        }

        [Column(IsVisible = Visibility.Visible,
            DisplayOrder = 50,
            IsReadOnly = true)]
        public long OrderId
        {
            get { return ProxyItem.OrderId; }
            set
            {
                ProxyItem.OrderId = value;
                RaisePropertyChanged("OrderId");
            }
        }

        [Column(IsVisible = Visibility.Hidden)]
        public bool IsVisible
        {
            get { return ProxyItem.ColumnItem != null ? true : false; }
        }

        public void ChangeOrderId(int orderId)
        {
            ProxyItem.OrderId = orderId;
            if (ProxyItem.ColumnItem != null)
            {
                ProxyItem.ColumnItem.DisplayIndex = orderId;
            }
            
        }
        
        [Column(IsVisible = Visibility.Collapsed)]
        public ColumnFilterItem ProxyItem { get; set; }
        public ColumnFilterGridItem(ColumnFilterItem proxyItem)
        {
            ProxyItem = proxyItem;
        }
    }
}
