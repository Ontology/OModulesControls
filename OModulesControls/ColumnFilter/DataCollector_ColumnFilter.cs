﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OModulesControls.ColumnFilter
{
    public class DataCollector_ColumnFilter
    {
        private List<OTreeNode> preNodes;
        public List<OTreeNode> Nodes { get; private set; }
        public List<OTreeNode> AllNodes { get; private set; }
        public List<ColumnFilterItem> FilterItemsOfLastTreeNode { get; private set; }

        private clsOntologyItem gridItem;

        private clsLocalConfig localConfig;

        private OntologyModDBConnector dbConnector_Nodes;
        private OntologyModDBConnector dbConnector_RefToNode;
        private OntologyModDBConnector dbConnector_FilterItems;
        private OntologyModDBConnector dbConnector_FilterItemAtts;
        private OntologyModDBConnector dbConnector_NewFilter;

        private clsTransaction transactionWorker;
        private clsRelationConfig relationConfig;

        public clsOntologyItem GetData_ColumnFilterTree(clsOntologyItem gridItem)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            Nodes.Clear();
            var rootNode = new OTreeNode(localConfig);
            
            Nodes.Add(rootNode);
            AllNodes = new List<OTreeNode>();
            AllNodes.Add(rootNode);
            
            result = dbConnector_Nodes.GetDataObjectsTree(localConfig.OItem_class_columnfilter, localConfig.OItem_class_columnfilter, localConfig.OItem_relationtype_contains);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var objectTree = dbConnector_Nodes.ObjectTree;

                var searchRefs = new List<clsObjectRel> 
                {
                    new clsObjectRel
                    {
                        ID_Other = gridItem.GUID,
                        ID_RelationType = localConfig.OItem_relationtype_belonging_resource.GUID,
                        ID_Parent_Object = localConfig.OItem_class_columnfilter.GUID
                    }
                };

                result = dbConnector_RefToNode.GetDataObjectRel(searchRefs);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    preNodes = dbConnector_RefToNode.ObjectRels.Select(refItem =>
                        new OTreeNode (new clsOntologyItem
                        {
                            GUID = refItem.ID_Object,
                            Name = refItem.Name_Object,
                            GUID_Parent = refItem.ID_Parent_Object,
                            Type = localConfig.Globals.Type_Object
                        }, gridItem,localConfig)).ToList();

                    
                    

                    preNodes.ForEach(nodeItem =>
                        {
                            GetParentNodes(nodeItem);
                            SetChildNodes(nodeItem);
                        });

                    rootNode.Nodes = rootNode.Nodes.OrderBy(nodeItem => nodeItem.Name).ToList();
                }

            }
            AllNodes.AddRange(preNodes);
            AllNodes.Where(nodeItem => nodeItem.Nodes != null && nodeItem.Nodes.Any()).ToList().ForEach(nodeItem =>
            {
                nodeItem.IsExpanded = true;
            });
            return result;
        }

        private void GetParentNodes(OTreeNode treeNode)
        {
            treeNode.AutomaticManagement = true;
            var parentNode = (from parentItem in dbConnector_Nodes.ObjectTree.Where(tn => tn.ID_Object == treeNode.Id)
                              join existing in preNodes on parentItem.ID_Object_Parent equals existing.Id into existings
                              from existing in existings.DefaultIfEmpty()
                              select new {parentItem, existing}).FirstOrDefault();
            while(parentNode != null)
            {
                OTreeNode parentTreeNode;
                if (parentNode.existing != null)
                {
                    parentTreeNode = parentNode.existing;
                }
                else
                {
                    parentTreeNode = new OTreeNode(new clsOntologyItem
                    {
                        GUID = parentNode.parentItem.ID_Object_Parent,
                        Name = parentNode.parentItem.Name_Object_Parent,
                        GUID_Parent = localConfig.OItem_class_columnfilter.GUID,
                        Type = localConfig.Globals.Type_Object
                    },gridItem, localConfig);
                    AllNodes.Add(parentTreeNode);
                }

                
                treeNode.ParentNode = parentTreeNode;
                
                parentNode = (from parentItem in dbConnector_Nodes.ObjectTree.Where(tn => tn.ID_Object == parentTreeNode.Id)
                              join existing in preNodes on parentItem.ID_Object_Parent equals existing.Id into existings
                              from existing in existings.DefaultIfEmpty()
                              select new { parentItem, existing }).FirstOrDefault();


                if (parentNode == null)
                {

                    if (parentTreeNode.ParentNode == null)
                    {
                        var rootNode = Nodes.First();
                        parentTreeNode.AutomaticManagement = true;
                        parentTreeNode.ParentNode = rootNode;
                        parentTreeNode.AutomaticManagement = false;
                        if (rootNode.Nodes == null)
                        {
                            rootNode.Nodes = new List<OTreeNode>();
                        }
                        rootNode.Nodes.Add(parentTreeNode);
                        
                    }
                    
                }
                
                
            }

            treeNode.AutomaticManagement = false;
            
        }

        public clsOntologyItem GetFilterItemsByTreeNode(OTreeNode treeNode, List<DataGridViewColumn> columnList)
        {
            FilterItemsOfLastTreeNode.Clear();

            var searchFilterItems = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = treeNode.Id,
                    ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                    ID_Parent_Other = localConfig.OItem_class_column__columnfilter_.GUID
                }
            };

            var result = dbConnector_FilterItems.GetDataObjectRel(searchFilterItems);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var searchAttributes = dbConnector_FilterItems.ObjectRels.Select(filterItem => new clsObjectAtt
                    {
                        ID_Object = filterItem.ID_Other,
                        ID_AttributeType = localConfig.OItem_attributetype_visible.GUID
                    }).ToList();

                if (searchAttributes.Any())
                {
                    result = dbConnector_FilterItemAtts.GetDataObjectAtt(searchAttributes);
                }

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    FilterItemsOfLastTreeNode = (from filterItem in dbConnector_FilterItems.ObjectRels
                                                 join attrib in dbConnector_FilterItemAtts.ObjAtts on filterItem.ID_Other equals attrib.ID_Object into attribs
                                                 from attrib in attribs.DefaultIfEmpty()
                                                 join col in columnList on filterItem.Name_Other equals col.DataPropertyName into cols
                                                 from col in cols.DefaultIfEmpty()
                                                 select new ColumnFilterItem (localConfig,
                                                     gridItem,
                                                     filterItem,
                                                     treeNode,
                                                     attrib,
                                                     false,
                                                     col)).ToList();
                }
            }

            return result;
        }

        public clsOntologyItem ExistsFilterWithSameName(clsOntologyItem parentItem, string newName)
        {
            var result = localConfig.Globals.LState_Success.Clone();
            if (parentItem != null)
            {
                var searchChild = new List<clsObjectRel> 
                { 
                    new clsObjectRel
                    {
                        ID_Object = parentItem.GUID,
                        ID_Parent_Other = localConfig.OItem_class_columnfilter.GUID,
                        Name_Other = newName,
                        ID_RelationType = localConfig.OItem_relationtype_contains.GUID
                    }
                };

                result = dbConnector_NewFilter.GetDataObjectRel(searchChild);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    return dbConnector_NewFilter.ObjectRels.Any(fil => fil.Name_Other.ToLower() == newName) ? localConfig.Globals.LState_Exists : localConfig.Globals.LState_Nothing;
                }
            }
            else
            {
                var searchFilter = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = localConfig.OItem_class_columnfilter.GUID,
                        Name = newName
                    }
                };

                result = dbConnector_NewFilter.GetDataObjects(searchFilter);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    return dbConnector_NewFilter.Objects1.Any(fil => fil.Name.ToLower() == newName) ? localConfig.Globals.LState_Exists : localConfig.Globals.LState_Nothing;
                }
            }
            

            return result;
        }

        private void SetChildNodes(OTreeNode treeNode)
        {
            var childNodes = (from subNode in dbConnector_Nodes.ObjectTree.Where(nodeItem => nodeItem.ID_Object_Parent == treeNode.Id)
                              join existingNode in preNodes on subNode.ID_Object equals existingNode.Id
                              select existingNode).OrderBy(nodeItem => nodeItem.Name).ToList();

            treeNode.Nodes = childNodes;
            childNodes.ForEach(childNode =>
                {
                    childNode.AutomaticManagement = true;
                    childNode.ParentNode = treeNode;
                    childNode.AutomaticManagement = false;
                    SetChildNodes(childNode);
                });
        }

        public DataCollector_ColumnFilter(clsLocalConfig localConfig, clsOntologyItem gridItem)
        {
            this.gridItem = gridItem;
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            Nodes = new List<OTreeNode>();
            FilterItemsOfLastTreeNode = new List<ColumnFilterItem>();
            dbConnector_Nodes = new OntologyModDBConnector(localConfig.Globals);
            dbConnector_RefToNode = new OntologyModDBConnector(localConfig.Globals);
            dbConnector_FilterItems = new OntologyModDBConnector(localConfig.Globals);
            dbConnector_FilterItemAtts = new OntologyModDBConnector(localConfig.Globals);
            dbConnector_NewFilter = new OntologyModDBConnector(localConfig.Globals);
            transactionWorker = new clsTransaction(localConfig.Globals);
            relationConfig = new clsRelationConfig(localConfig.Globals);
        }
    }
}
