﻿using OModulesControls.BaseClasses;
using OModulesControls.Grid;
using OModulesControls.ViewModelUtils;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;

namespace OModulesControls.ColumnFilter
{
    public class ViewModel_ColumnFilter : ViewModelBase
    {
        private bool NotSetColumnAttributes;
        private clsLocalConfig localConfig;
        private DataCollector_ColumnFilter dataCollector_ColumnFilter;
        private List<ColumnDisplayOrder> DisplayOrder;
        private Globals globals;

        private double opacityButtonOrderTD = 0.5;
        private double opacityButtonOrderBU = 0.5;

        private bool enabledButtonOrderTD = false;
        private bool enabledButtonOrderBU = false;

        public List<object> VisibleRowItems { get; set; }


        private OTreeNode currSelItem;
        public OTreeNode CurrSelItem 
        {
            get { return currSelItem; }
            set 
            { 
                currSelItem = value;
                RaisePropertyChanged("CurrSelItem");
            }
        }

        private ColumnFilterGridItem selectedGridViewItem;
        public object SelectedGridViewItem
        {
            get { return selectedGridViewItem; }
            set 
            {
                selectedGridViewItem = (ColumnFilterGridItem)value;
                RaisePropertyChanged("SelectedGridViewItem");
            }
        }

        public bool EnabledButton_OrderTD
        {
            get { return enabledButtonOrderTD; }
            set
            {
                enabledButtonOrderTD = value;
                OpacityButtonOrderTD = enabledButtonOrderTD ? 1 : 0.5;
                RaisePropertyChanged("EnableButton_OrderTD");
            }
        }

        public bool EnabledButton_OrderBU
        {
            get { return enabledButtonOrderBU; }
            set
            {
                enabledButtonOrderBU = value;
                OpacityButtonOrderBU = enabledButtonOrderBU ? 1 : 0.5;
                RaisePropertyChanged("EnableButton_OrderBU");
            }
        }

        public double OpacityButtonOrderTD
        {
            get { return opacityButtonOrderTD; }
            set
            {
                opacityButtonOrderTD = value;
                RaisePropertyChanged("OpacityButtonOrderTD");
            }
        }

        public double OpacityButtonOrderBU
        {
            get { return opacityButtonOrderBU; }
            set
            {
                opacityButtonOrderBU = value;
                RaisePropertyChanged("OpacityButtonOrderBU");
            }
        }

        public Globals Globals
        {
            get { return globals; }
            set
            {
                globals = value;
                

                localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
                if (localConfig == null)
                {
                    localConfig = new clsLocalConfig(new Globals());
                    LocalConfigManager.AddLocalConfig(localConfig);
                }

                Nodes.Add(new OTreeNode(localConfig));
                dataCollector_ColumnFilter = new DataCollector_ColumnFilter(localConfig, gridItem);

                RaisePropertyChanged("Nodes");
            }
        }

        private List<DataGridViewColumn> gridColumnList;
        public List<DataGridViewColumn> GridColumnList
        {
            get { return gridColumnList; }
            set 
            {
                gridColumnList = value;
            }
        }

        private List<OTreeNode> nodes;
        public List<OTreeNode> Nodes
        {
            get { return nodes; }
            set 
            {
                nodes = value;
                RaisePropertyChanged("Nodes");
            }
        }

        private ObservableCollection<ColumnFilterGridItem> columnFilterList;
        public ObservableCollection<ColumnFilterGridItem> ColumnFilterList
        {
            get { return columnFilterList; }
            set
            {
                columnFilterList = value;

                RaisePropertyChanged("ColumnFilterList");
            }
        }

        private void SetDisplayOrderList()
        {
            
            var properties = (typeof(ColumnFilterGridItem)).GetProperties().ToList();
            DisplayOrder = properties.Select(prop => 
            {
                var columnAttribute = (ColumnAttribute)prop.GetCustomAttributes().Where(custAtt => custAtt.GetType() == typeof(ColumnAttribute)).FirstOrDefault();
                if (columnAttribute != null)
                {
                    if (columnAttribute.IsVisible == Visibility.Visible)
                    {
                        return new ColumnDisplayOrder
                        {
                            ColumnName = prop.Name,
                            OrderId = columnAttribute.DisplayOrder
                        };
                    }
                    else
                    {
                        return new ColumnDisplayOrder
                        {
                            ColumnName = prop.Name,
                            OrderId = -1
                        };
                    }
                    
                }
                else
                {
                    return new ColumnDisplayOrder
                    {
                        ColumnName = prop.Name,
                        OrderId = -1
                    };
                }
            }).ToList();

            int orderId = 0;
            DisplayOrder.Where(dord => dord.OrderId > -1).OrderBy(dord => dord.OrderId).ThenBy(dord => dord.ColumnName).ToList().ForEach(dord =>
                {
                    dord.OrderId = orderId;
                    orderId++;
                });

            DisplayOrder.Where(dord => dord.OrderId == -1).ToList().ForEach(dord =>
                {
                    dord.OrderId = orderId;
                    orderId++;
                });
            
        }


        private clsOntologyItem gridItem;
        public clsOntologyItem GridItem
        {
            get { return gridItem; }
            set 
            { 
                gridItem = value;
                var result = dataCollector_ColumnFilter.GetData_ColumnFilterTree(gridItem);
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    Nodes = dataCollector_ColumnFilter.Nodes;
                }
            }
        }

        public ViewModel_ColumnFilter()
        {
            Nodes = new List<OTreeNode>();
            ColumnFilterList = new ObservableCollection<ColumnFilterGridItem>();
            VisibleRowItems = new List<object>();
            SetDisplayOrderList();
        }

        public void SetDataGridViewColumnAttributes()
        {
            if (GridColumnList != null && GridColumnList.Any() && ColumnFilterList.Any())
            {
                int orderId = 0;
                (from gridColumn in GridColumnList
                 join columnFilter in ColumnFilterList on gridColumn.DataPropertyName equals columnFilter.NameColumnFilter into columnFilters
                 from columnFilter in columnFilters.DefaultIfEmpty()
                 select new { gridColumn, columnFilter }).OrderBy(col => col.columnFilter != null ? col.columnFilter.OrderId : col.gridColumn.DisplayIndex).ToList().ForEach(col =>
                 {
                     col.gridColumn.Visible = col.columnFilter != null ? col.columnFilter.VisibleColumn : false;
                     if (col.columnFilter != null)
                     {
                         col.gridColumn.DisplayIndex = orderId;
                         orderId++;
                     }
                     
                 });
            }
        }
        public Visibility IsColumnVisible(DataGridAutoGeneratingColumnEventArgs e)
        {
            
            var property = (typeof(ColumnFilterGridItem)).GetProperties().Where(prop => prop.Name == e.PropertyName).FirstOrDefault();

            if (property != null)
            {
                var columnAttribute = (ColumnAttribute)property.GetCustomAttributes().Where(custAtt => custAtt.GetType() == typeof(ColumnAttribute)).FirstOrDefault();

                if (columnAttribute != null)
                {
                    return columnAttribute.IsVisible;
                }
            }

            return Visibility.Collapsed;
        }

        public int GetDisplayOrder(DataGridAutoGeneratingColumnEventArgs e)
        {
            var orderItem = DisplayOrder.Where(dord => dord.ColumnName == e.PropertyName).FirstOrDefault();

            if (orderItem != null)
            {
                return orderItem.OrderId > -1 ? orderItem.OrderId : 0;
            }
            return 0;
        }

        
        public bool CancelEdit(DataGridBeginningEditEventArgs e)
        {
            var property = (typeof(ColumnFilterGridItem)).GetProperties().Where(prop => prop.Name == e.Column.SortMemberPath).FirstOrDefault();

            if (property != null)
            {
                var columnAttribute = (ColumnAttribute)property.GetCustomAttributes().Where(custAtt => custAtt.GetType() == typeof(ColumnAttribute)).FirstOrDefault();

                if (columnAttribute != null)
                {
                    return columnAttribute.IsReadOnly;
                }
            }
            return false;
        }

        public void GetFilterItemsByTreeNode(OTreeNode treeNode)
        {
            if (treeNode.Name == localConfig.Globals.Root.Name && GridColumnList != null && GridColumnList.Any())
            {

                var columnFilterList = GridColumnList.Select(col => new ColumnFilterItem(localConfig.Globals.NewGUID,
                    col.DataPropertyName,
                    col.DisplayIndex,
                    col.Visible,
                    col)).ToList();
                ColumnFilterList = new ObservableCollection<ColumnFilterGridItem>(columnFilterList.Select(cf => new ColumnFilterGridItem(cf)).OrderBy(fi => fi.OrderId));
            }
            else
            {
                var result = dataCollector_ColumnFilter.GetFilterItemsByTreeNode(treeNode, GridColumnList);
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    ColumnFilterList = new ObservableCollection<ColumnFilterGridItem>(dataCollector_ColumnFilter.FilterItemsOfLastTreeNode.Select(cf => new ColumnFilterGridItem(cf)).OrderBy(fi => fi.OrderId));
                    //SetDisplayOrderList();
                }
            }
            
        }

        public void ConfigGridButtons(SelectionChangedEventArgs e)
        {
            EnabledButton_OrderBU = false;
            EnabledButton_OrderTD = false;
            if (e.AddedItems.Count == 1)
            {
                var selectedItem = (ColumnFilterGridItem)e.AddedItems[0];
                if (ColumnFilterList.Any(cf => cf.OrderId < selectedItem.OrderId))
                {
                    EnabledButton_OrderBU = true;
                }

                if (ColumnFilterList.Any(cf => cf.OrderId > selectedItem.OrderId))
                {
                    EnabledButton_OrderTD = true;
                }
            }

        }

        public void GridViewItemUp()
        {
            if (selectedGridViewItem != null)
            {
                var itemBefore = columnFilterList.Select(cf => cf.OrderId < selectedGridViewItem.OrderId).OrderByDescending(cf => cf).FirstOrDefault();

                if (itemBefore != null)
                {

                }
            }
            
        }

        public void RefreshTreeView()
        {
            RaisePropertyChanged("Nodes");
        }

        public clsOntologyItem CreateFilter()
        {
            var result = localConfig.Globals.LState_Success.Clone();
            //if (currSelItem != null)
            //{
            //    var frmName = new frm_Name("Create Filter", Globals);
            //    frmName.ShowDialog();
            //    if (frmName.DialogResult == DialogResult.OK)
            //    {
            //        var filterName = frmName.Value1;
            //        if (currSelItem.Name == localConfig.Globals.Root.Name)
            //        {
            //            result = dataCollector_ColumnFilter.ExistsFilterWithSameName(null, filterName);
            //            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            //            {

            //            }
            //            else if (result.GUID == localConfig.Globals.LState_Exists.GUID)
            //            {
            //                System.Windows.MessageBox.Show("Es existiert bereits ein Element mit dem Namen " + filterName, "Filter vorhanden", MessageBoxButton.OK, MessageBoxImage.Information);
            //            }
            //        }
            //        else
            //        {

            //        }
            //    }

                
            //}

            return result;
        }


        private static void TreeViewItemSelectedChangedCallBack(TreeViewHelper.DependencyPropertyEventArgs e)
        {
            if (e != null && e.DependencyPropertyChangedEventArgs.NewValue != null)
            {

            }

        }
    }
}
