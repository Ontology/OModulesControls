﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesControls.ColumnFilter
{
    public class ColumnDisplayOrder
    {
        public int OrderId { get; set; }
        public string ColumnName { get; set; }
    }
}
