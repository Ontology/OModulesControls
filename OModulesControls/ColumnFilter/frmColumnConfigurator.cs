﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OModulesControls.ColumnFilter
{
    public partial class frmColumnConfigurator : Form
    {
        private clsLocalConfig localConfig;

        private clsOntologyItem parentItem;

        private DataGridView dgvParent;

        public frmColumnConfigurator(Globals globals, clsOntologyItem parentItem, DataGridView dgvParent)
        {
            InitializeComponent();

            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            this.parentItem = parentItem;
            this.dgvParent = dgvParent;


            var columns = dgvParent.Columns.Cast<DataGridViewColumn>().ToList();

            var userControl_ColumnFilter = (UserControl_ColumnFilter)elementHost_ColumnFilter.Child;
            userControl_ColumnFilter.Globals = localConfig.Globals;
            userControl_ColumnFilter.GridItem = parentItem;
            userControl_ColumnFilter.GridColumns = columns;
        }

        private void toolStripButton_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
