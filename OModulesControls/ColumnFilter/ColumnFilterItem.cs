﻿using OModulesControls.BaseClasses;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using OntologyAppDBConnector;

namespace OModulesControls.ColumnFilter
{
    public class ColumnFilterItem : NotifyPropertyChange
    {
        private bool batchUpdate;

        private clsLocalConfig localConfig;

        private clsOntologyItem refItem;
        public clsOntologyItem RefItem
        {
            get { return refItem; }
            set
            {
                refItem = value;
            }
        }

        public delegate void SaveResult(clsOntologyItem result);
        public event SaveResult saveResult;

        public string IdColumnFilter 
        {
            get { return filterItemRel.ID_Other; }
            set
            {
                filterItemRel.ID_Other = value;
            }
        } 
        public string NameColumnFilter 
        {
            get { return filterItemRel.Name_Other; }
            set
            {
                filterItemRel.Name_Other = value;
            }
        }

        private DataGridViewColumn columnItem;
        public DataGridViewColumn ColumnItem 
        {
            get { return columnItem; }
            set
            {
                columnItem = value;
            }
        }

        public bool VisibleColumn 
        {
            get { return visibleAttribute != null ? (bool)visibleAttribute.Val_Bit : false; } 
            set
            {
                if (visibleAttribute != null)
                {
                    visibleAttribute.Val_Bit = value;
                }
                else
                {
                    visibleAttribute = localConfig.RelationConfigurator.Rel_ObjectAttribute(filterItem,
                        localConfig.OItem_attributetype_visible,
                        value);
                }

                if (!batchUpdate)
                {
                    var result = SaveVisibility();

                    if (saveResult != null)
                    {
                        saveResult(result);
                    }
                }
               
            }
        }
        public OTreeNode ParentNode {get; set;}
        public long OrderId 
        {
            get { return filterItemRel.OrderID != null ? (long)filterItemRel.OrderID : 0; }
            set
            {
                filterItemRel.OrderID = value;

                if (!batchUpdate)
                {
                    var result = SaveFilterItemToFilter();
                    if (saveResult != null)
                    {
                        saveResult(result);
                    }
                }
                
            }
        }
        private clsObjectAtt visibleAttribute;
        public clsObjectAtt VisibleAttribute
        { 
            get { return visibleAttribute; }
            set
            {
                visibleAttribute = value;
            }
        }
        private clsObjectRel filterItemRel;
        public clsObjectRel FilterItemRel
        {
            get { return filterItemRel; }
            set
            {
                filterItemRel = value;

                var result = SaveFilterItemToFilter();
                if (!batchUpdate)
                {
                    if (saveResult != null)
                    {
                        saveResult(result);
                    }
                }
                
            }
        }

        private clsOntologyItem filter;
        public clsOntologyItem Filter
        {
            get 
            {
                if (filterItemRel != null)
                {
                    return new clsOntologyItem
                    {
                        GUID = filterItemRel.ID_Object,
                        Name = filterItemRel.Name_Object,
                        GUID_Parent = filterItemRel.ID_Parent_Object,
                        Type = filterItemRel.Ontology
                    };
                }
                else
                {
                    return null;
                }
            }
            set
            {
                filter = value;

            }
        }

        private clsOntologyItem filterItem;
        public clsOntologyItem FilterItem
        {
            get 
            {
                if (filterItemRel != null)
                {
                    return new clsOntologyItem
                    {
                        GUID = filterItemRel.ID_Other,
                        Name = filterItemRel.Name_Other,
                        GUID_Parent = filterItemRel.ID_Parent_Other,
                        Type = filterItemRel.Ontology
                    };
                }
                else
                {
                    return null;
                }
            }
            set
            {
                filter = value;

            }
        }

        public clsOntologyItem SaveFilterItem()
        {
            var result = localConfig.Globals.LState_Success.Clone();

            localConfig.TransactionWorker.ClearItems();
            result = localConfig.TransactionWorker.do_Transaction(filterItem);

            return result;
        }

        public clsOntologyItem  SaveVisibility()
        {
            var result = localConfig.Globals.LState_Success.Clone();

            localConfig.TransactionWorker.ClearItems();

            result = localConfig.TransactionWorker.do_Transaction(visibleAttribute);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                if (visibleAttribute.ID_Attribute == null)
                {
                    visibleAttribute = localConfig.TransactionWorker.OItem_Last.OItemObjectAtt.Clone();
                }
            }


            return result;
        }

        public clsOntologyItem SaveFilterItemToFilter()
        {
            var result = localConfig.Globals.LState_Success.Clone();

            localConfig.TransactionWorker.ClearItems();

            result = localConfig.TransactionWorker.do_Transaction(filterItemRel);

            return result;
        }

        public ColumnFilterItem(clsLocalConfig localConfig,
            clsOntologyItem refItem,
            clsObjectRel filterItemRel, 
            OTreeNode treeNode, 
            clsObjectAtt visibleAttribute = null, 
            bool batchUpdate = false,
            DataGridViewColumn column = null)
        {
            this.refItem = refItem;

            this.localConfig = localConfig;

            this.filterItemRel = filterItemRel;

            this.visibleAttribute = visibleAttribute;

            this.batchUpdate = batchUpdate;

            this.ParentNode = treeNode;

            this.ColumnItem = column;
        }

        public ColumnFilterItem(clsLocalConfig localConfig,
           clsOntologyItem refItem,
           clsOntologyItem filter,
           clsOntologyItem filterItem,
           bool visible,
           OTreeNode treeNode,
           bool batchUpdate = false,
           DataGridViewColumn column = null)
        {
            this.refItem = refItem;

            this.localConfig = localConfig;

            filterItemRel = localConfig.RelationConfigurator.Rel_ObjectRelation(filter, filterItem, localConfig.OItem_relationtype_contains);

            visibleAttribute = localConfig.RelationConfigurator.Rel_ObjectAttribute(filterItem, localConfig.OItem_attributetype_visible, visible);
            
            this.batchUpdate = batchUpdate;

            this.ParentNode = treeNode;

            this.columnItem = column;
        }

        public ColumnFilterItem(string idFilterItem,
                    string nameFilterItem,
                    long orderId,
                    bool visible,
                    DataGridViewColumn colItem)
        {
            batchUpdate = true;
            filterItemRel = new clsObjectRel
            {
                ID_Other = idFilterItem,
                Name_Other = nameFilterItem,
                OrderID = orderId
            };

            visibleAttribute = new clsObjectAtt
            {
                Val_Bit = visible
            };

            this.columnItem = colItem;
        }
       
    }
}
