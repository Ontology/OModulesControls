﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OModulesControls
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private clsLocalConfig objLocalConfig;
        
        public MainWindow()
        {
            DataContext = new Globals();
            InitializeComponent();
            columnFilter.Globals = (Globals) DataContext;
            objLocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (objLocalConfig == null)
            {
                objLocalConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(objLocalConfig);
            }

            columnFilter.GridItem = new clsOntologyItem
            {
                GUID = "4744643d9df248829250bff3659d5741",
                Name = "Audible Hörbücher",
                GUID_Parent = "f18f0dc8e57b41f0afbf8a0edd4c0fa8",
                Type = objLocalConfig.Globals.Type_Object
            };

            
            //columnFilter.GetData(new clsOntologyItem
            //    {
            //        GUID_Parent = "4546225bab044036b2182e95a25a0c2b"
            //    });
        }
    }
}
