﻿using OModulesControls.BaseClasses;
using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesControls.Dialog
{
    public class ViewModel_WindowName : ViewModelBase
    {
        private string value1;
        public string Value1
        {
            get { return value1; }
            set
            {
                value1 = value;
                RaisePropertyChanged("Value1");
            }
        }

        private string value2;
        public string Value2
        {
            get { return value2; }
            set
            {
                value2 = value;
                RaisePropertyChanged("Value2");
            }
        }

        private Globals globals;
        public Globals Globals
        {
            get { return globals; }
            set
            {
                globals = value;
                RaisePropertyChanged("Globals");
            }
        }
    }
}
