﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OModulesControls.Dialog
{
    /// <summary>
    /// Interaction logic for Window_Name.xaml
    /// </summary>
    public partial class Window_Name : Window
    {
        public Window_Name()
        {
            InitializeComponent();
        }
    }
}
