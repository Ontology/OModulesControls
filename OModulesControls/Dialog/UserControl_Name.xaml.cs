﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OModulesControls.Dialog
{
    /// <summary>
    /// Interaction logic for UserControl_Name.xaml
    /// </summary>
    public partial class UserControl_Name : UserControl
    {
        private ViewModel_Name viewModel;

        public static readonly DependencyProperty GlobalsProperty = DependencyProperty.Register
            (
                 "Globals",
                 typeof(Globals),
                 typeof(UserControl_Name),
                 new PropertyMetadata(null)
            );

        private Globals globals;
        public Globals Globals
        {
            get { return globals; }
            set
            {
                globals = value;
                viewModel = (ViewModel_Name)DataContext;
                viewModel.Globals = globals;
            }
        }

        public static readonly DependencyProperty Value1Property = DependencyProperty.Register
            (
                 "Value1",
                 typeof(string),
                 typeof(UserControl_Name),
                 new PropertyMetadata(null)
            );

        private string value1;
        public string Value1
        {
            get { return value1; }
            set
            {
                value1 = value;
            }
        }

        public static readonly DependencyProperty Value2Property = DependencyProperty.Register
            (
                 "Value2",
                 typeof(string),
                 typeof(UserControl_Name),
                 new PropertyMetadata(null)
            );

        private string value2;
        public string Value2
        {
            get { return value2; }
            set
            {
                value2 = value;
            }
        }

        public UserControl_Name()
        {
            InitializeComponent();
        }
    }
}
