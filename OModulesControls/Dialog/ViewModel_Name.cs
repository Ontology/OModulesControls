﻿using OModulesControls.BaseClasses;
using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesControls.Dialog
{
    public class ViewModel_Name : ViewModelBase
    {
        private string menuItemEdit;
        public string MenuItemEdit
        {
            get { return menuItemEdit; }
            set
            {
                menuItemEdit = value;
                RaisePropertyChanged("MenuItemEdit");
            }
        }

        private Globals globals;
        public Globals Globals
        {
            get { return globals; }
            set
            {
                globals = value;
                RaisePropertyChanged("Globals");
            }
        }

        public ViewModel_Name()
        {
            menuItemEdit = "x_Edit";
        }
    }
}
