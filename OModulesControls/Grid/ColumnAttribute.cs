﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OModulesControls.Grid
{
    public class ColumnAttribute : Attribute
    {
        public Visibility IsVisible { get; set; }
        public string ColumnHeaderKey { get; set; }
        public string ColumnHeader { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsReadOnly { get; set; }
    }
}
