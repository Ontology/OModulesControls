﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OntologyAppDBConnector;
using ImportExport_Module;
using OntologyClasses.BaseClasses;
using System.Reflection;
using System.Windows.Forms;
using OntologyClasses.Interfaces;
using System.Runtime.InteropServices;

namespace OModulesControls
{
    public class clsLocalConfig : ILocalConfig
    {
        private const string cstrID_Ontology = "b77e1ee72553443f8a7f02a6948c17f8";
        private ImportWorker objImport;

        public Globals Globals { get; set; }

        private clsOntologyItem objOItem_DevConfig = new clsOntologyItem();
        public clsOntologyItem OItem_BaseConfig { get; set; }

        private OntologyModDBConnector objDBLevel_Config1;
        private OntologyModDBConnector objDBLevel_Config2;
	
		public clsOntologyItem OItem_attributetype_visible { get; set; }
public clsOntologyItem OItem_class_column__columnfilter_ { get; set; }
public clsOntologyItem OItem_class_columnfilter { get; set; }
public clsOntologyItem OItem_relationtype_belonging_resource { get; set; }
public clsOntologyItem OItem_relationtype_contains { get; set; }

public clsTransaction TransactionWorker { get; private set; }
public clsRelationConfig RelationConfigurator { get; private set; }

	private void get_Data_DevelopmentConfig()
        {
            var objORL_Ontology_To_OntolgyItems = new List<clsObjectRel> {new clsObjectRel {ID_Object = cstrID_Ontology, 
                                                                                             ID_RelationType = Globals.RelationType_contains.GUID, 
                                                                                             ID_Parent_Other = Globals.Class_OntologyItems.GUID}};

            var objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds:false);
            if (objOItem_Result.GUID == Globals.LState_Success.GUID)
            {
                if (objDBLevel_Config1.ObjectRels.Any())
                {

                    objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel {ID_Object = oi.ID_Other,
                                                                                                                                ID_RelationType = Globals.RelationType_belongingAttribute.GUID}).ToList();

                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel {ID_Object = oi.ID_Other,
                                                                                                                                    ID_RelationType = Globals.RelationType_belongingClass.GUID}));
                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel {ID_Object = oi.ID_Other,
                                                                                                                                    ID_RelationType = Globals.RelationType_belongingObject.GUID}));
                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingRelationType.GUID
                    }));

                    objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds:false);
                    if (objOItem_Result.GUID == Globals.LState_Success.GUID)
                    {
                        if (!objDBLevel_Config2.ObjectRels.Any())
                        {
                            throw new Exception("Config-Error");
                        }
                    }   
                    else
                    {
                        throw new Exception("Config-Error");
                    }
                }
                else
                {
                    throw new Exception("Config-Error");
                }

            }

        }
  
	public clsLocalConfig()
        {
            Globals = new Globals();
            set_DBConnection();
            get_Config();
        }

        public clsLocalConfig(Globals Globals)
        {
            this.Globals = Globals;
            set_DBConnection();
            get_Config();
        }
  
	private void set_DBConnection()
        {
		    objDBLevel_Config1 = new OntologyModDBConnector(Globals);
		    objDBLevel_Config2 = new OntologyModDBConnector(Globals);
            TransactionWorker = new clsTransaction(Globals);
            RelationConfigurator = new clsRelationConfig(Globals);
			objImport = new ImportWorker(Globals);
        }
  
	private void get_Config()
        {
            try
            {
                get_Data_DevelopmentConfig();
                get_Config_AttributeTypes();
                get_Config_RelationTypes();
                get_Config_Classes();
                get_Config_Objects();
            }
            catch(Exception ex)
            {
                var objAssembly = Assembly.GetExecutingAssembly();
                AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[]) objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                var strTitle = "Unbekannt";
                if (objCustomAttributes.Length == 1) 
                {
                    strTitle = objCustomAttributes.First().Title;
                }
                if (MessageBox.Show(strTitle + ": Die notwendigen Basisdaten konnten nicht geladen werden! Soll versucht werden, sie in der Datenbank " +
                          Globals.Index + "@" + Globals.Server + " zu erzeugen?", "Datenstrukturen",MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    var objOItem_Result = objImport.ImportTemplates(objAssembly);
                    if (objOItem_Result.GUID != Globals.LState_Error.GUID)
                    {
                        get_Data_DevelopmentConfig();
                        get_Config_AttributeTypes();
                        get_Config_RelationTypes();
                        get_Config_Classes();
                        get_Config_Objects();
                    }
                    else
                    {
                        throw new Exception("Config not importable");
                    }
                }
                else
                {
                    Environment.Exit(0);
                }
            }
        }
  
	private void get_Config_AttributeTypes()
        {
		var objOList_attributetype_visible = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "attributetype_visible".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                           select objRef).ToList();

            if (objOList_attributetype_visible.Any())
            {
                OItem_attributetype_visible = new clsOntologyItem()
                {
                    GUID = objOList_attributetype_visible.First().ID_Other,
                    Name = objOList_attributetype_visible.First().Name_Other,
                    GUID_Parent = objOList_attributetype_visible.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }


	}
  
	private void get_Config_RelationTypes()
        {
		var objOList_relationtype_belonging_resource = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "relationtype_belonging_resource".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                           select objRef).ToList();

            if (objOList_relationtype_belonging_resource.Any())
            {
                OItem_relationtype_belonging_resource = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belonging_resource.First().ID_Other,
                    Name = objOList_relationtype_belonging_resource.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belonging_resource.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

var objOList_relationtype_contains = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "relationtype_contains".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                           select objRef).ToList();

            if (objOList_relationtype_contains.Any())
            {
                OItem_relationtype_contains = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_contains.First().ID_Other,
                    Name = objOList_relationtype_contains.First().Name_Other,
                    GUID_Parent = objOList_relationtype_contains.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }


	}
  
	private void get_Config_Objects()
        {
		
	}
  
	private void get_Config_Classes()
        {
		var objOList_class_column__columnfilter_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "class_column__columnfilter_".ToLower() && objRef.Ontology == Globals.Type_Class
                                           select objRef).ToList();

            if (objOList_class_column__columnfilter_.Any())
            {
                OItem_class_column__columnfilter_ = new clsOntologyItem()
                {
                    GUID = objOList_class_column__columnfilter_.First().ID_Other,
                    Name = objOList_class_column__columnfilter_.First().Name_Other,
                    GUID_Parent = objOList_class_column__columnfilter_.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

var objOList_class_columnfilter = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "class_columnfilter".ToLower() && objRef.Ontology == Globals.Type_Class
                                           select objRef).ToList();

            if (objOList_class_columnfilter.Any())
            {
                OItem_class_columnfilter = new clsOntologyItem()
                {
                    GUID = objOList_class_columnfilter.First().ID_Other,
                    Name = objOList_class_columnfilter.First().Name_Other,
                    GUID_Parent = objOList_class_columnfilter.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }


	}

    public string IdLocalConfig
    {
        get
        {
            var attrib =
                  Assembly.GetExecutingAssembly()
                      .GetCustomAttributes(true)
                      .FirstOrDefault(objAttribute => objAttribute is GuidAttribute);
            if (attrib != null)
            {
                return ((GuidAttribute)attrib).Value;
            }
            else
            {
                return null;
            }
        }
    }
    }

}